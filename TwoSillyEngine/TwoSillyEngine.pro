QT += core
QT -= gui

CONFIG += c++11

TARGET = TwoSillyEngine
CONFIG += console
CONFIG -= app_bundle

win32{
    LIBS += -lwsock32
   }
TEMPLATE = app

SOURCES += \
    tool/function.cpp \
    tool/gettime.cpp \
    tool/logoutput.cpp \
    tool/socket.cpp \
    utils/Config.cpp \
    utils/PointMatrix.cpp \
    utils/polygon.cpp \
    src/Beegive.cpp \
    src/bridge.cpp \
    src/comb.cpp \
    src/Engine.cpp \
    src/gcodeExport.cpp \
    src/infill.cpp \
    src/inset.cpp \
    src/layerPart.cpp \
    src/optimizedModel.cpp \
    src/pathOrderOptimizer.cpp \
    src/polygonOptimizer.cpp \
    src/Processor.cpp \
    src/raft.cpp \
    src/skin.cpp \
    src/skirt.cpp \
    src/slicer.cpp \
    src/support.cpp \
    src/timeEstimate.cpp \
    modelFile/modelFile.cpp \
    libs/clipper/clipper.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    tool/function.h \
    tool/gettime.h \
    tool/logoutput.h \
    tool/settings.h \
    tool/socket.h \
    tool/string.h \
    utils/Config.h \
    utils/floatpoint.h \
    utils/intpoint.h \
    utils/PointMatrix.h \
    utils/polygon.h \
    src/Beegive.h \
    src/bridge.h \
    src/comb.h \
    src/gcodeExport.h \
    src/infill.h \
    src/inset.h \
    src/layerPart.h \
    src/multiVolumes.h \
    src/optimizedModel.h \
    src/pathOrderOptimizer.h \
    src/polygonOptimizer.h \
    src/Processor.h \
    src/raft.h \
    src/skin.h \
    src/skirt.h \
    src/sliceDataStorage.h \
    src/slicer.h \
    src/support.h \
    src/SupportDataStorage.h \
    src/timeEstimate.h \
    modelFile/modelFile.h \
    libs/clipper/clipper.hpp
